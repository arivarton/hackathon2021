from django.urls import include, path, re_path
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from . import views

router = routers.DefaultRouter()
router.register(r'members', views.MembersViewSet)
router.register(r'api/get/members_regdate/?$', views.MembersRegdateViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    re_path(r'api-token-auth/?$', obtain_auth_token, name='api_token_auth'), 
    re_path(r'api/get/member_count/?$', views.CountUsers.as_view()),
    re_path(r'api/get/avg_age/?$', views.AvgUsersAge.as_view()),
    re_path(r'api/get/study_direction_percentage/(?P<direction>.*)/?$', views.StudyDirectionPercentage.as_view()),
    re_path(r'api/change_password/(?P<email>.*)/?$', views.ChangePassword.as_view()),
    re_path(r'api/change_study_direction/(?P<email>.*)/?$', views.ChangeStudyDirection.as_view()),

    re_path(r'api/change/(?P<email>.*)/?$', views.EditUserAPI.as_view()),
    re_path(r'api/delete/user/(?P<email>.*)/?$', views.DeleteAPI.as_view()),
]
