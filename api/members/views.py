#  from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from .serializers import MembersSerializer, MembersRegdateSerializer
from .models import Members
from .models import study_direction_categories


import bcrypt


class MembersViewSet(viewsets.ModelViewSet):
    queryset = Members.objects.all()
    serializer_class = MembersSerializer

class MembersRegdateViewSet(viewsets.ModelViewSet):
    queryset = Members.objects.all()
    serializer_class = MembersRegdateSerializer

class CountUsers(APIView):
    """
    Count all registered users
    """

    def get(self, request, format=None):
        """
        Return a list of all users.
        """

        return Response(len(Members.objects.all()))


class AvgUsersAge(APIView):
    """
    View to list all users in the system.
    """

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        ages = [i.age() for i in Members.objects.all()]

        return Response(sum(ages)/len(ages))


class StudyDirectionPercentage(APIView):
    """
    View to list all users in the system.
    """

    def get(self, request, direction, format=None):
        """
        Return a list of all users.
        """
        study_direction_categories = [i[0] for i in Members.study_direction.field.choices]
        if direction in study_direction_categories:
            response = str(Members.objects.filter(study_direction=direction).count()/Members.objects.all().count()*100) + '%'
        else:
            response = f'{direction} is not a valid choice, choose from {study_direction_categories}.'

        return Response(response)


class ChangePassword(APIView):
    """
    Change user password.
    
    curl example:
    curl http://localhost:8000/api/change_password/<email> -X PUT -d "password=tralala" -H "Authorization: Token $token"

    * Requires token authentication.
    * Only admin users are able to access this view.

    To get token do this (in a linux shell and parson json with python):
    token="$(curl http://localhost:8000/api-token-auth/ -X POST -d "username=kodesonen&password=1234" | python -c "import sys, json; print(json.load(sys.stdin)['token'])")"
    """

    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAdminUser]

    def put(self, request, email, format=None):
        """
        Change password
        """
        if 'password' in request.data:
            try:
                user = Members.objects.get(email=email)
                password = bytes(request.data['password'], 'utf-8')
                p = bcrypt.hashpw(password, bcrypt.gensalt())
                user.password = p.decode('utf-8')
                user.save()
                response = f'Password for {user.name} with email address {user.email} has been changed.'
            except Members.DoesNotExist:
                response = 'Email not found.'
        else:
            response = 'Missing password parameter.'

        return Response(response)


class ChangeStudyDirection(APIView):
    """
    Change study direction of user.

    Same token procedure as changing password
    
    curl example:
    curl http://localhost:8000/api/change/study_direction/<email> -X PUT -d "study_direction=tralala" -H "Authorization: Token $token"

    Study direction must be a valid choice.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAdminUser]

    def put(self, request, email, format=None):
        """
        Change study direction.
        """
        if 'study_direction' in request.data:
            try:
                study_direction_categories = [i[0] for i in Members.study_direction.field.choices]
                if request.data['study_direction'] in study_direction_categories:
                    user = Members.objects.get(email=email)
                    user.study_direction = request.data['study_direction']
                    user.save()
                    response = f'Study direction for {user.name} with email address {user.email} has been changed.'
                else:
                    response = f'{request.data["study_direction"]} is not a valid choice, choose from {study_direction_categories}.'
            except Members.DoesNotExist:
                response = 'Email not found.'
        else:
            response = 'Missing study_direction parameter.'

        return Response(response)


class EditUserAPI(APIView):
    """
    Change study direction of user.

    Same token procedure as changing password
    
    curl example:
    curl http://localhost:8000/api/change/study_direction/<email> -X PUT -d "study_direction=tralala" -H "Authorization: Token $token"

    Study direction must be a valid choice.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAdminUser]

    def put(self, request, email, format=None):
        """
        Change study direction.
        """
        if 'study_direction' in request.data:
            try:
                study_direction_categories = [i[0] for i in Members.study_direction.field.choices]
                if request.data['study_direction'] in study_direction_categories:
                    user = Members.objects.get(email=email)
                    user.study_direction = request.data['study_direction']
                    user.save()
                    response = f'Study direction for {user.name} with email address {user.email} has been changed.'
                else:
                    response = f'{request.data["study_direction"]} is not a valid choice, choose from {study_direction_categories}.'
            except Members.DoesNotExist:
                response = f'{email} not found.'

        if 'password' in request.data:
            try:
                user = Members.objects.get(email=email)
                password = bytes(request.data['password'], 'utf-8')
                p = bcrypt.hashpw(password, bcrypt.gensalt())
                user.password = p.decode('utf-8')
                user.save()
                response = f'Password for {user.name} with email address {user.email} has been changed.'
            except Members.DoesNotExist:
                response = '{email} not found.'

        return Response(response)


class DeleteAPI(APIView):
    """
    Delete a user by email

    Same token procedure as changing password
    
    curl example:
    curl http://localhost:8000/api/delete/user/<email> -X DELETE -H "Authorization: Token $token"

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAdminUser]

    def delete(self, request, email, format=None):
        try:
            user = Members.objects.get(email=email)
            user.delete()
            response = f'{email} deleted.'

        except Members.DoesNotExist:
            response = 'Email not found.'

        return Response(response)
