from rest_framework import serializers

from .models import Members

class MembersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Members
        fields = ('name', 'email', 'study_direction')

class MembersRegdateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Members
        fields = ('name', 'regdate')
