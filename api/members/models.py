from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

import datetime

study_direction_categories = (
        ('DATAING', 'Data ingenior'),
        ('ELEKING', 'Elektro ingenior'),
        ('MASKING', 'Maskin ingenior'),
        ('ANNET', 'Annet'))

# Create your models here.
class Members(models.Model):
    name = models.CharField(max_length=64)
    email = models.CharField(max_length=64)
    study_direction = models.CharField(max_length=64, choices=study_direction_categories, default='ANNET')
    degree = models.CharField(max_length=64, default=None)
    admin = models.PositiveSmallIntegerField(default=0)
    password = models.CharField(max_length=256, default=None)
    regdate = models.DateField(max_length=64)
    birthyear = models.IntegerField(validators=[MinValueValidator(1800), MaxValueValidator(datetime.date.today().year)])

    def __str__(self):
        return f'{self.name} - {self.email}'

    def age(self):
        return datetime.date.today().year - self.birthyear
