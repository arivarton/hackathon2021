
[[_TOC_]]

# Oppsett for Django
```
pip install -r requirements.txt -r requirements-dev.txt
cd api
python manage.py migrate
python manage.py loaddata fixtures/populate.yaml
python manage.py createsuperuser kodesonen
```

# Superbruker
Brukernavn: kodesonen

Mail: kontakt@kodesonen.no

Passord: 1234

# Kjør testserver
```
python manage.py runserver_plus
```

# API Bruk
### Medlemsliste
```
curl http://localhost:8000/members/ -X GET
```

### Totalt antall medlemmer
```
curl http://localhost:8000/api/get/member_count -X GET
```

### Gjennomsnitts alder
```
curl http://localhost:8000/api/get/avg_age -X GET
```

### Prosentandel av studieretning (valgfri retning)
```
curl -X GET http://localhost:8000/api/get/study_direction_percentage/DATAING
```


#### Henting av token
```
token="$(curl http://localhost:8000/api-token-auth/ -X POST -d "username=kodesonen&password=1234" | python -c "import sys, json; print(json.load(sys.stdin)['token'])")"
```

## Endring av brukere (krever autentisering med token)

### Endre studieretning
```
curl http://localhost:8000/api/change/<email> -X PUT -d "study_direction=tralala" -H "Authorization: Token $token"
```

### Passord bytte
```
curl http://localhost:8000/api/change/<email> -X PUT -d "password=tralala" -H "Authorization: Token $token"
```

### Kombinering av verdiendringer
```
curl http://localhost:8000/api/change/<email> -X PUT -d "password=tralala&study_direction=jejeje" -H "Authorization: Token $token"
```

## Slette bruker (krever autentisering med token)
```
curl http://localhost:8000/api/delete/user/<email> -X DELETE -H "Authorization: Token $token"
```
